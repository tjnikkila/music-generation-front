# browser-midi

This project plays midi notes in browser. Browser gets midi notes from backend. Backend uses machine learning to generate notes. Frontend sends user action to backend. Backend uses user action to generate midi note.


## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

# Probably no need to run these

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```
